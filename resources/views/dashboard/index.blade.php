@extends('dashboard.main')

@section('content')

<div id="wrapper" style="background-color: black;">
  <ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: white;" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="row sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="p-0 sidebar-brand-icon">
          <i class="bi bi-person-circle"></i>
        </div>
        <div class="p-0 sidebar-brand-text">Wijaya Putra</div>
        <div class="p-0 sidebar-brand-text">Admin</div>
    </a>


    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href='{{ route('dashboard') }}'>
            <i class="bi bi-speedometer2 bi-color"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href='{{ route('datawebsite') }}' data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="bi bi-clipboard-data bi-color"></i>
            <span>Database Website</span>
        </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ route('tdw') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Database Website</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ route('aplikasi') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-table bi-color"></i>
          <span>Tabel Aplikasi dan OPD</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ route('masterapp') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Master Aplikasi</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-gear-fill bi-color"></i>
          <span>Setting</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-box-arrow-left bi-color"></i>
          <span>Keluar</span>
      </a>
    </li>
  </ul>
</div>

@endsection

@section('subjudul')
<span class="judul-dashboard">Dashboard</span>
<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Statistik Pengakses Website</span>
@endsection

@section('datepicker')
<div class="datepicker">
  <input type="date" name="dateofbirth" class="startdate">
  <span style="color:black;font-weight:100" class="px-3">To</span>
  <input type="date" name="dateofbirth" class="enddate">
</div>

@endsection

@section('content2')
<p id="statistik">Statistik User</p>

<div id="chart">
</div>

<script>
  var options = {
    chart: {
      height: 350,
      type: "line",
      stacked: false
    },
    dataLabels: {
      enabled: false
    },
    colors: ["#247BA0"],
    series: [
      {
        name: "Series B",
        data: [300, 1000, 300, 800]
      }
    ],
    stroke: {
      width: [4, 4]
    },
    plotOptions: {
      bar: {
        columnWidth: "20%"
      }
    },
    xaxis: {
      categories: ['1 Nov', '10 Nov', '20 Nov', '30 Nov']
    },
    yaxis: [
      {
        axisTicks: {
          show: true
        },
        axisBorder: {
          show: true,
          color: "#247BA0"
        },
        labels: {
          style: {
            colors: "#247BA0"
          }
        },
        title: {
          style: {
            color: "#247BA0"
          }
        }
      }
    ],
    tooltip: {
      shared: false,
      intersect: true,
      x: {
        show: false
      }
    },
    legend: {
      horizontalAlign: "left",
      offsetX: 40
    }
  };
  
  var chart = new ApexCharts(document.querySelector("#chart"), options);
  
  chart.render();
</script>
@endsection

@section('content3')

<div>
  <div class="rec-up align-items-center d-flex"></div>
  <div class="rectangle align-items-center">
    <p id="statistik">Overall Statistik</p>
    <div id="chartpie">
    </div>
    <script>
      var options = {
          series: [70, 40, 20, 5],
          // labels: ["Apple", "Mango", "Banana", "Papaya"]
          chart: {
          type: 'donut',
        },
        responsive: [{
          breakpoint: 300,
          options: {
            chart: {
              width: 50
            },
            legend: {
              position: 'bottom'
            }
          }
        }],
        };

        var chart = new ApexCharts(document.querySelector("#chartpie"), options);
        chart.render();
    </script>
  </div>
</div>

@endsection