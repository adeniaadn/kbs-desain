<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/style.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('/css/style1.css') }}"> --}}
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    
    <title>KBS</title>
  </head>
  <body>
    <div class="d-flex p-0 container-fluid">
      <div class="d-flex flex-column bd-highlight sidebar">

        <main>
            @yield('content')
        </main>
        
      </div>
      <div class="d-flex flex-column bd-highlight content">
        <div class="p-2 bd-highlight">
          <div class="d-flex align-items-center bd-highlight">
            <div class="flex-grow-1 justify-content-center bd-highlight search">
              <form class="d-flex py-2 px-3">
                <input class="form-control me-2 search-admin" type="search" placeholder="Telurusi website ini" aria-label="Search">
                <button class="btn btn-outline btn-search-admin" type="submit">
                  <i class="bi bi-search icon-search"></i>
                </button>
              </form>
            </div>
            <div class="px-2 pe-3 d-flex justify-content-center bd-highlight">
              <i class="bi bi-bell-fill icon-bell"></i>
            </div>
          </div>
        </div>
        <div class="ps-4 bd-highlight">
          <div class="d-flex align-items-center bd-highlight header">
            
            <main>
                @yield('subjudul')
            </main>
          </div>
          <main>
            @yield('datepicker')
          </main>
          
          <div>
            <div class="rec-up align-items-center d-flex"></div>
            <div class="rectangle align-items-center">
            <main >
                @yield('content2')
            </main>
            </div>
          </div>
          <main >
            @yield('content3')
          </main>
        
        </div>
        <div class="p-2 bd-highlight"></div>
      </div>
    </div>



    <!-- Bootstrap JS -->
    <script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
  </body>
</html>