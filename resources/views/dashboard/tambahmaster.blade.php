@extends('dashboard.main')

@section('subjudul')
<span class="judul-dashboard">Aplikasi dan OPD</span>
<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Tambah Master Aplikasi</span>

@endsection

@section('content')
<div id="wrapper" style="background-color: black;">
  <ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: white;" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="row sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="p-0 sidebar-brand-icon">
          <i class="bi bi-person-circle"></i>
        </div>
        <div class="p-0 sidebar-brand-text">Wijaya Putra</div>
        <div class="p-0 sidebar-brand-text">Admin</div>
    </a>


    <!-- Nav Item - Dashboard -->
    <li class="nav-item ">
        <a class="nav-link" href='{{ route('dashboard') }}'>
            <i class="bi bi-speedometer2 bi-color"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href='{{ route('datawebsite') }}' data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="bi bi-clipboard-data bi-color"></i>
            <span>Database Website</span>
        </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link collapsed" href='{{ route('tdw') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Database Website</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ route('aplikasi') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-table bi-color"></i>
          <span>Tabel Aplikasi dan OPD</span>
      </a>
    </li>
    <li class="nav-item active">
      <a class="nav-link collapsed" href='{{ route('masterapp') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Master Aplikasi</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-gear-fill bi-color"></i>
          <span>Setting</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-box-arrow-left bi-color"></i>
          <span>Keluar</span>
      </a>
    </li>
  </ul>
</div>

@endsection

@section('content2')
          
              <form class="form">
                <div class="row mb-3">
                  <label for="inputEmail3" class="col-sm-2 col-form-label text-dark judul">Master Aplikasi :</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input" placeholder="Nama Laporan">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputPassword3" class="col-sm-2 col-form-label text-dark judul">Pemilik Aplikasi :</label>
                  <div class="col-sm-10 dropdown">
                    <select name="opd" id="FMS">
                      <option value=""></option>
                      <option value="" disabled selected hidden>Pilih OPD</option>
                      <option value="opd1">Diskominfos</option>
                      <option value="opd2">Diskominfos</option>
                    </select>
                  </div>
                </div>
                
                <div class="button">
                  <button type="submit" class="btn btn-primary">Batal</button>
                  <button type="button" class="btn btn-primary" id="simpan" onclick="location.href='{{ route('datawebsite') }}'">Simpan</button>
                </div>
                
              </form>
            </div>
          </div>
@endsection