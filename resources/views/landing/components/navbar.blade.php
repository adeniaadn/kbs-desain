@include('landing.modals.personalData')
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light mx-3">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/user/welcome') }}">
            <img class="logo-brand" src="{{ asset('assets/img/logo-brand.png') }}" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav align-items-center ms-auto mb-lg-0">
                <li class="nav-item">
                    <a class="pe-1 nav-link" href="https://www.denpasarkota.go.id/">
                        <img class="logo" src="{{ asset('assets/img/logo-kota-denpasar-bali 2.png') }}" alt="Logo Kota Denpasar">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="pe-1 nav-link" href="https://gianyarkab.go.id/">
                        <img class="logo" src="{{ asset('assets/img/logo-kabupaten-gianyar-bali 2.png') }}" alt="Logo Kab. Gianyar">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="pe-1 nav-link" href="https://klungkungkab.go.id/">
                        <img class="logo" src="{{ asset('assets/img/logo-kabupaten-klungkung-bali 2.png') }}" alt="Logo Kab. Klungkung">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="pe-1 nav-link" href="https://bulelengkab.go.id/">
                        <img class="logo" src="{{ asset('assets/img/logo-kabupaten-buleleng-bali 2.png') }}" alt="Logo Kab. Buleleng">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="pe-1 nav-link" href="https://banglikab.go.id/">
                        <img class="logo" src="{{ asset('assets/img/logo-kabupaten-bangli-bali 2.png') }}" alt="Logo Kab. Bangli">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="pe-1 nav-link" href="https://www.tabanankab.go.id/">
                        <img class="logo" src="{{ asset('assets/img/logo-kabupaten-Tabanan-bali 2.png') }}" alt="Logo Kab. Tabanan">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="pe-1 nav-link" href="https://badungkab.go.id/">
                        <img class="logo" src="{{ asset('assets/img/logo-kabupaten-badung-bali 2.png') }}" alt="Logo Kab. Badung">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="pe-1 nav-link" href="http://www.karangasemkab.go.id/">
                        <img class="logo" src="{{ asset('assets/img/logo-kabupaten-karangasem-bali 2.png') }}" alt="Logo Kab. Karangasem">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="pe-1 nav-link" href="https://jembranakab.go.id/">
                        <img class="logo" src="{{ asset('assets/img/logo-kabupaten-jembrana-bali 2.png') }}" alt="Logo Kab. Jembrana">
                    </a>
                </li>
            </ul>
            <div class="d-flex mx-3 line-black"></div>
            <div class="d-flex btn-chat">
                <button class="btn btn-outline-success" type="button" data-bs-toggle="modal" data-bs-target="#personalData">
                    <i class="bi bi-chat-dots-fill"></i>
                    Tanya Admin
                </button>
            </div>
        </div>
    </div>
</nav>