@extends('landing.layouts.app')
@section('content')
<style>
    p.not-found {
        color: black;
        font-style: normal;
    }
    #search-result{
        height: 100vh;
    }
</style>
<!-- Content search result -->
<section id="search-result" class="d-flex align-items-start my-3">
    <div class="container" >
        <div class="row">
            <div class="col-lg-8">
                <form class="input-group mb-3" action="post">
                    <input type="search" class="form-control input-search-result" placeholder="Telusuri website ini" aria-label="Recipient's username" aria-describedby="button-addon2">
                    <span class="p-0 input-group-text line-white-search">|</span>
                    <button class="btn btn-secondary btn-search-result" type="button" id="button-addon2"><i class="bi bi-search"></i></button>
                </form>
            </div>
        </div>
        <!-- Nav category -->
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-pills my-4" id="pills-tab" role="tablist">
                    <li class="pe-5 nav-item" role="presentation">
                        <button class="nav-link disabled" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#semua" type="button" role="tab" aria-controls="pills-home" aria-selected="true">
                            Semua
                        </button>
                    </li>
                    <li class="pe-5 nav-item" role="presentation">
                        <button class="nav-link disabled" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#gambar" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">
                            Gambar
                        </button>
                    </li>
                    <li class="pe-5 nav-item" role="presentation">
                        <button class="nav-link disabled" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#video" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">
                            Video
                        </button>
                    </li>
                    <li class="pe-5 nav-item" role="presentation">
                        <button class="nav-link disabled" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#dokumen" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">
                            Dokumen
                        </button>
                    </li>
                </ul>

            </div>
        </div>
        <!-- Not found -->
        <div class="row">
            <div class="pt-5 d-flex flex-column align-items-center ">
                <div class="p-2 bd-highlight">
                    <p class="not-found" style="font-size: 3rem; color:#00415A; margin-bottom:0px;">
                        <i class="bi bi-exclamation-circle-fill"></i>
                    </p>
                </div>
                <div class="p-2 bd-highlight">
                    <p class="mb-1 not-found" style="font-weight: 600; font-size: 20px; line-height: 10px;">
                        Kata Kunci Tidak Dapat Ditemukan
                    </p>
                </div>
                <div class="p-2 bd-highlight">
                    <p class="not-found" style="font-weight: 400; font-size: 13px;">
                        Silahkan ajukan pertanyaan Anda <span data-bs-toggle="modal" data-bs-target="#personalData" style="text-decoration: underline blue; color:blue; cursor:pointer;">disini</span> 
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection