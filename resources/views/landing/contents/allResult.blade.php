@extends('landing.layouts.app')
@section('content')
    <!-- Content search result -->
    <section id="search-result" class="d-flex align-items-start my-3">
        <div class="container" >
            <div class="row">
                <div class="col-lg-8">
                    <form class="input-group mb-3" action="post">
                        <input type="search" class="form-control input-search-result" placeholder="Telusuri website ini" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <span class="p-0 input-group-text line-white-search">|</span>
                        <button class="btn btn-secondary btn-search-result" onclick="location.href='{{ url('/user/not-found') }}'" type="button" id="button-addon2"><i class="bi bi-search"></i></button>
                    </form>
                </div>
            </div>
            <!-- Nav category -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills my-4" id="pills-tab" role="tablist">
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link active" onclick="location.href='{{ url('/user/search-results') }}'" type="button" aria-selected="true">
                                Semua
                            </button>
                        </li>
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link" onclick="location.href='{{ url('/user/search-results-gambar') }}'" type="button" aria-selected="false">
                                Gambar
                            </button>
                        </li>
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link" onclick="location.href='{{ url('/user/search-results-video') }}'" type="button" aria-selected="false">
                                Video
                            </button>
                        </li>
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link" onclick="location.href='{{ url('/user/search-results-document') }}'" type="button" aria-selected="false">
                                Dokumen
                            </button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="semua" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="col-lg-8 tagline-search">
                                <p>
                                    https://www.denpasarkota.go.id > page 
                                </p>
                                <a href="https://www.denpasarkota.go.id">
                                    <h5>
                                        Syarat Pembuatan KTP - Website Portal Resmi Pemerintah Kota Denpasar
                                    </h5>
                                </a>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                            </div>
                            <div class="col-lg-8 tagline-search">
                                <p>
                                    https://disdukcapil.badungkab.go.id > pelayanan-kartu-tanda-... 
                                </p>
                                <a href="https://disdukcapil.badungkab.go.id">
                                    <h5>
                                        Pelayanan Kartu Tanda Penduduk - Website Resmi Dinas Kabupaten ...
                                    </h5>
                                </a>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                            </div>
                            <div class="col-lg-8 tagline-search">
                                <p>
                                    https://dukcapil.jembranakab.go.id > 
                                </p>
                                <a href="https://dukcapil.jembranakab.go.id">
                                    <h5>
                                        Dinas DukCapil - Pemerintah Kabupaten Jembrana
                                    </h5>
                                </a>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                            </div>
                            <div class="col-lg-8 tagline-search">
                                <p>
                                    https://disdukcapil.klungkungkab.go.id > category > layanan... 
                                </p>
                                <a href="https://disdukcapil.klungkungkab.go.id">
                                    <h5>
                                        Layanan - Dinas Kependudukan dan Pencatatan Sipil
                                    </h5>
                                </a>
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Penelusuran terkait -->
            <div class="row mt-4">
                <div class="col-lg-12">                        
                    <h5>Penelusuran Terkait</h5>
                </div>
                <div class="col-md-7 d-flex flex-wrap btn-penelusuran">
                    <button type="button" class="mt-3 me-5 btn btn-secondary btn-md"><i class="pe-2 bi bi-search"></i>
                        Cara <span class="fw-bold">membuat KTP</span> online    
                    </button>
                    <button type="button" class="mt-3 me-5 btn btn-secondary btn-md"><i class="pe-2 bi bi-search"></i>
                        Cara <span class="fw-bold">membuat KTP</span> 2022    
                    </button>
                    <button type="button" class="mt-3 me-5 btn btn-secondary btn-md"><i class="pe-2 bi bi-search"></i>
                        Cara <span class="fw-bold">membuat KTP</span> Terbaru    
                    </button>
                    <button type="button" class="mt-3 me-5 btn btn-secondary btn-md"><i class="pe-2 bi bi-search"></i>
                        Syarat <span class="fw-bold">membuat KTP</span> baru    
                    </button>
                </div>
            </div>
            <!-- Pagination -->
            <div class="row my-5 d-flex">
                <nav aria-label="pagination" class="d-flex justify-content-center">
                    <ul class="pagination">
                        <li class="p-2 page-item active">
                            <a class="page-link" href="#">1</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">2</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">3</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">4</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">5</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">Berikutnya<i class="bi bi-chevron-right icon-arrow"></i></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
@endsection