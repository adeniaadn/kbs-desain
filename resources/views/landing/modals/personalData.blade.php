<style>
    .form-control.personal-data{
        height: 36px;

        border-radius: 10px;
    }
</style>

<!-- Modal personal data -->
<div class="modal fade" id="personalData" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header m-2">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="border: 2px solid #00415A;"></button>
            </div>
            <form action="post">
                <div class="modal-body">
                    <div class="container col-9">
                        <div class="row">
                            <h5 class="text-center modal-title">
                                Untuk Mengakses Halaman Ini,
                                Harap Masukan Data Diri Anda
                                Terlebih Dahulu
                            </h5>
                        </div>
                        <div class="row my-3 d-flex justify-content-center">
                            <div class="col-10 my-1">
                                <label class="form-label">Nama Lengkap</label>
                                <input type="input" class="form-control personal-data" value="" aria-describedby="namaLengkap" placeholder="Putri">    
                            </div>
                            <div class="col-10 my-1">
                                <label class="form-label">Email</label>
                                <input type="email" class="form-control personal-data" value="" aria-describedby="email" placeholder="Putri1234@example.com">    
                            </div>
                            <div class="col-10 my-1">
                                <label class="form-label">No. Telepon</label>
                                <input type="text" class="form-control personal-data" value="" aria-describedby="noTelepon" placeholder="0818181818181">    
                            </div>
                            <div class="col-10 my-1">
                                <label class="form-label">Alamat</label>
                                <input type="text" class="form-control personal-data" value="" aria-describedby="alamat" placeholder="Jalan PB Sudirman no 1 Denpasar">    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-send mb-4">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>