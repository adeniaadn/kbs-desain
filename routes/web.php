<?php

use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/admin/main', function () {
//     return view('dashboard.main');
// })->name('main');

// auth

// dashboard
    Route::get('/admin/dashboard', function () {
        return view('dashboard.dashboard');
    });
// main kerangka
Route::get('/dashboard', function () {
    return view('dashboard.main');
})->name('main');

//dashboard
Route::get('/', function () {
    return view('dashboard.index');
})->name('dashboard');

//tabel data website
Route::get('/admin/datawebsite', function () {
    return view('dashboard.tabeldatawebsite');
})->name('datawebsite');

// tambah data website
Route::get('/admin/tambahdatawebsite', function () {
    return view('dashboard.tdw');
})->name('tdw');

// aplikasi dan OPD
Route::get('/admin/aplikasiopd', function () {
    return view('dashboard.aplikasi');
})->name('aplikasi');

// tambah master aplikasi
Route::get('/admin/tambahmaster', function () {
    return view('dashboard.tambahmaster');
})->name('masterapp');

// embed aplikasi
Route::get('/admin/embedApp', function () {
    return view('dashboard.embed');
})->name('embed');
// landing
    Route::get('/user/welcome', function () {
        return view('landing.contents.welcome');
    });
    Route::get('/user/search-results', function () {
        return view('landing.contents.allResult');
    });
    Route::get('/user/search-results-gambar', function () {
        return view('landing.contents.allResultGambar');
    });
    Route::get('/user/search-results-video', function () {
        return view('landing.contents.allResultVideo');
    });
    Route::get('/user/search-results-document', function () {
        return view('landing.contents.allResultDocument');
    });
    Route::get('/user/chat-admin', function () {
        return view('landing.contents.chat');
    });
    Route::get('/user/not-found', function () {
        return view('landing.contents.notFound');
    });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

